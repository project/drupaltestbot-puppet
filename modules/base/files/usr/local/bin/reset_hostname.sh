#!/bin/bash
NOTIFY_EMAILS=testbots@association.drupal.org

if hostname | grep "^ip-"
then
	PREVIOUS_HOSTNAME=$(hostname)
	HOSTNAME=$(curl -fs http://169.254.169.254/latest/meta-data/public-hostname/) \
	&& IPADDR=$(curl -fs http://169.254.169.254/latest/meta-data/local-ipv4/)
	if ! test -z "$HOSTNAME" && ! test -z "$IPADDR"; then
	  echo $HOSTNAME >/etc/hostname
	  hostname $HOSTNAME
	  echo "127.0.0.1 localhost localhost.localdomain
	  $IPADDR  $HOSTNAME" >/etc/hosts
	fi

	echo "
Testbot hostname update for $(hostname) (was $PREVIOUS_HOSTNAME)
http://$(hostname)

Add it to qa.drupal.org at
basic: https://qa.drupal.org/user/12898/pifr/add
isntall: https://qa.drupal.org/user/41178/pifr/add

" | mail -s "Testbot hostname $(hostname) updated (was $PREVIOUS_HOSTNAME)" $NOTIFY_EMAILS

fi

