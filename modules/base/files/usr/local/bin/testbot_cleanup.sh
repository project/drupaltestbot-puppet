#!/bin/bash -x

# You can do anything you want as a cleanup or fixup here
# Any time this file changes it will be rerun on all testbots.

# If a host still has hostname "ip-*" then reset it and send out mail.
# Seems that Amazon isn't responding correctly on early creation?
/usr/local/bin/reset_hostname.sh

# Make sure that pifr is on 6.x-3.x and latest
cd /var/lib/drupaltestbot/sites/all/modules/project_issue_file_review && git fetch --all && git checkout 6.x-3.x && git reset --hard HEAD && git pull

# drush -r /var/lib/drupaltestbot vset pifr_client_server https://qa.drupal.org/
drush -r /var/lib/drupaltestbot vset pifr_client_timeout 60

touch /etc/drupaltestbot/.db_loaded

# Clean up hosts file comments with leading space. This seems to break
# the puppet hosts provider.
perl -pi.bak -e 's/^[\t ]+#.*$//' /etc/hosts

# apt-get update && apt-get upgrade -y

# One-time only - force site rename of all testbots. This should be removed
# after the site names have been set to include the instance.
# rm /etc/drupaltestbot/.site_name_set

# Disable update status module
drush -r /var/lib/drupaltestbot dis update -y

date >>/tmp/testbot_cleanup_ran.txt

