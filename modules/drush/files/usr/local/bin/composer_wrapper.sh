#!/bin/bash
set -o pipefail
/usr/bin/php -d "apc.enable_cli=0" /usr/local/bin/composer "$@"
