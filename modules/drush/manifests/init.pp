class drush {
  exec { "drush clone":
    command => 'git clone https://github.com/drush-ops/drush.git',
    require => [ Package['ca-certificates'], Package['git'] ],
    cwd => '/opt',
    creates => '/opt/drush/.git',
    path => '/usr/bin',
  }

  exec { "drush checkout":
    command => "git fetch --all && git checkout ${drush_commit}",
    cwd => '/opt/drush',
    require => Exec['drush clone'],
    path => '/bin:/usr/bin:/usr/local/bin',
    unless => "git describe --all | grep ${drush_commit} || git rev-parse HEAD | grep ${drush_commit}",
    notify => Exec['composer update for drush checkout'],
  }

  exec { "install composer":
    command => 'curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer',
    cwd => '/tmp',
    creates => '/usr/local/bin/composer',
    path => '/usr/bin:/bin',
  }

  file { "/usr/local/bin/composer_wrapper.sh":
    source => "puppet:///modules/drush/usr/local/bin/composer_wrapper.sh",
    mode => 0755,
    require => File['/usr/local/bin'],
  }

  exec { "composer install for drush":
    command => "composer_wrapper.sh install",
    cwd => "/opt/drush",
    require => [Exec["drush checkout"],Exec['install composer'],File['/usr/local/bin/composer_wrapper.sh']],
    path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    creates => "/opt/drush/vendor/phpunit",
    environment => ["COMPOSER_HOME=/root"],
    # logoutput => true,
  }

  exec { "composer update for drush checkout":
    command => "composer_wrapper.sh self-update && composer_wrapper.sh update",
    cwd => "/opt/drush",
    require => Exec["composer install for drush"],
    path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    environment => ["COMPOSER_HOME=/root"],
    refreshonly => true,
  }

  file { "/usr/bin/drush":
    ensure => link,
    target => '/opt/drush/drush',
    require => Exec['drush checkout'],
  }

  # Make sure that any old drush versions are gone from /usr/local
  file { ["/usr/local/bin/drush", "/usr/local/lib/drush"]:
    ensure => absent,
    force => true,
  }
}
