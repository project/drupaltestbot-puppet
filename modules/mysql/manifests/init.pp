class mysql {

  package { "maatkit":
    ensure => present,
  }

  package { "libmysqlclient18":
    ensure => '5.5.34',
  }

  class server {
    if $architecture == 'ppc64' {
      $mysql_package = 'mysql-server-5.5'
    } else {
      $mysql_package = 'mariadb-server-5.5'
    }
    package { "$mysql_package":
      ensure => present,
    }

    service { "mysql":
      enable => true,
      require => Package["$mysql_package"],
      hasrestart => true,
    }

    file { "/etc/mysql/my.cnf":
      owner   => root,
      group   => root,
      mode    => 755,
      source  => "puppet:///modules/mysql/my.cnf",
      require => Package["$mysql_package"],
      notify  => Service["mysql"],
    }

    file { "/etc/mysql/conf.d/tuning.cnf":
      owner   => root,
      group   => root,
      mode    => 755,
      source  => "puppet:///modules/mysql/tuning.cnf",
      require => Package["$mysql_package"],
      notify  => Service["mysql"],
    }
  }
}
